import UIKit
import MapKit
import SwiftDate

class MapViewController: UIViewController {
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var tableView: UITableView!
    
    let LocationCellIdentifier = "LocationCell"
    var annotations: [MKAnnotation] = []
    
    let store: DataStore = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.store
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up view controller
        title = "Recent Data"
        
        setupTableViewDelegate()
    }
    
    @IBAction func clearCache() {
        print("Clearing cache!")
        store.clear()
    }
    
    deinit {
        store.delegate = nil
    }
}

extension MapViewController: UITableViewDelegate, UITableViewDataSource {
    func setupTableViewDelegate() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: LocationCellIdentifier)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
}
