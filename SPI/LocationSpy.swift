import Foundation
import CoreLocation

class LocationSpy : NSObject, CLLocationManagerDelegate {
    var previousLocation: CLLocation? = nil
    let store: DataStore
    
    init(withStore store: DataStore) {
        self.store = store
        
        // Get the most recent location from the store
        let locations = store.filter(type: DataFilterType.location)
        
        if let recent = locations.last {
            // JANK: I should be using generics for the datum type
            switch recent.data {
            case .location(let loc):
                previousLocation = CLLocation(latitude: loc.lat, longitude: loc.lon)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations.count == 0 {
            return
        }
        
        let latestLocation = locations[locations.count - 1]
        
        print("Latest: \(latestLocation.coordinate.latitude), \(latestLocation.coordinate.longitude)")
        
        // Make sure this new location is > 10 meters from the previous
        if let prev = previousLocation {
            print("Prev: \(prev.coordinate.latitude), \(prev.coordinate.longitude) (d: \(prev.distance(from: latestLocation)))")

            let distance = prev.distance(from: latestLocation)
            if distance < 50 {
                print("Skipping new location with dist: \(distance)")
                return
            }
        }

        previousLocation = latestLocation
        let datum = Datum(fromLocation: latestLocation)
        store.add(datum: datum)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location manager failed with error")
        print(error)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        print("Authorization changed!")
    }
}
