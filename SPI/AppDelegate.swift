import UIKit
import CoreLocation
import UserNotifications
import HealthKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    let store: DataStore
    let locationManager = CLLocationManager()
    let locationSpy: LocationSpy
    let notificationCenter = UNUserNotificationCenter.current()
    
    let storePath: URL = {
        let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let documentsDirectoryPath = NSURL(fileURLWithPath: documentsDirectoryPathString)
        return documentsDirectoryPath.appendingPathComponent("db.json")!
    }()
    
    override init() {
        // Load the data store, seeding it with our JSON file
        let fileManager = FileManager.default
        
        if !fileManager.isReadableFile(atPath: storePath.path) {
            let emptyArray = "[]"
            let created = fileManager.createFile(atPath: storePath.path, contents: emptyArray.data(using: .utf8), attributes: nil)
            if created {
                print("Data store created!")
            } else {
                print("Error creating datastore")
                exit(1)
            }
        }
        
        var jsonData: Data
        do {
            jsonData = try Data(contentsOf: storePath)
        } catch {
            print("Could not read db file")
            exit(1)
        }
        
        do {
            let decoder = JSONDecoder()
            store = try decoder.decode(DataStore.self, from: jsonData)
        } catch {
            print("Could not parse JSON:", error)
            exit(1)
        }
        
        self.locationSpy = LocationSpy(withStore: self.store)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        locationManager.delegate = locationSpy
        
        // Enable location services...
        if !CLLocationManager.locationServicesEnabled() {
            // Location services are disabled
            print("Location services disabled")
            return true
        }
        
        let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus != .authorizedWhenInUse && authorizationStatus != .authorizedAlways {
            // Need to ask for permission
            print("Requesting authorization...")
            locationManager.requestAlwaysAuthorization()
        }

        
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startMonitoringSignificantLocationChanges()
        
        // Enable notifications
        notificationCenter.requestAuthorization(options: [.alert, .sound], completionHandler: { (granted, error) in
            if !granted {
                print("Notification access not granted...")
            }
        })
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

