import Foundation
import CoreLocation
import SwiftyJSON
import MapKit
import UserNotifications
import Alamofire

let ApiUrl = "https://daters-api.teif.xyz/data"
//let ApiUrl = "http://localhost:3000/data"


////
// Various data types that can be uploaded
//

struct LocationData : Codable {
    var lat: Double
    var lon: Double
    var alt: Double
    var hAcc: Double
    var vAcc: Double
    var course: Double
    var speed: Double
}

enum DataType {
    case location(LocationData)
    
    func toString() -> String {
        switch self {
        case .location(_):
            return "location"
        }
    }
    
    func toFilterType() -> DataFilterType {
        switch self {
        case .location(_):
            return DataFilterType.location
        }
    }
}

enum DataFilterType {
    case location
}

extension DataType : Encodable {
    func encode(to encoder: Encoder) throws {
        switch self {
        case .location(let loc):
            try loc.encode(to: encoder)
        }
    }
}

////
// The container class, Datum, which wraps any of the various datatypes we collect
//

class Datum: Codable {
    var data: DataType
    var recordedAt: Int
    var submitted: Bool
    var source: String
    let id = UUID().uuidString
    
    enum EncodingError: Error {
        case invalidDataType(type: String)
    }
    
    init(fromLocation location: CLLocation) {
        let locationData = LocationData(
            lat: location.coordinate.latitude,
            lon: location.coordinate.longitude,
            alt: location.altitude,
            hAcc: location.horizontalAccuracy,
            vAcc: location.verticalAccuracy,
            course: location.course,
            speed: location.speed
        )
        self.source = "spi"
        self.data = DataType.location(locationData)
        self.recordedAt = Int(location.timestamp.timeIntervalSince1970)
        self.submitted = false
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        recordedAt = try values.decode(Int.self, forKey: .recorded_at)
        submitted = try values.decode(Bool.self, forKey: .submitted)
        source = try values.decode(String.self, forKey: .source)
        let dataType = try values.decode(String.self, forKey: .data_type)
        
        switch dataType {
        case "location":
            let locData = try values.decode(LocationData.self, forKey: .data)
            data = DataType.location(locData)
        default:
            throw EncodingError.invalidDataType(type: dataType)
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(data, forKey: .data)
        try container.encode(recordedAt, forKey: .recorded_at)
        try container.encode(data.toString(), forKey: .data_type)
        try container.encode(source, forKey: .source)
        try container.encode(submitted, forKey: .submitted)
    }
    
    enum CodingKeys: String, CodingKey {
        case data_type
        case data
        case recorded_at
        case source
        case submitted
    }
}

protocol DataStoreDelegate {
    func dataStoreDidUpdate()
}


////
// The store itself, which contains many data points
//

class DataStore : NSObject, Codable {
    var data: [Datum] = []
    var delegate: DataStoreDelegate?
    let storePath: URL = {
        let documentsDirectoryPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let documentsDirectoryPath = NSURL(fileURLWithPath: documentsDirectoryPathString)
        return documentsDirectoryPath.appendingPathComponent("db.json")!
    }()
    
    required init(from decoder: Decoder) throws {
        var values = try decoder.unkeyedContainer()
        
        while !values.isAtEnd {
            let val = try values.decode(Datum.self)
            data.append(val)
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.unkeyedContainer()
        try data.forEach { (datum) in
            try container.encode(datum)
        }
    }
    
    func add(datum: Datum) {
        data.append(datum)
        
        data.sort { (a, b) -> Bool in
            return a.recordedAt < b.recordedAt
        }
        
        // Prune old data
        if data.count > 150 {
            print("Pruning old data (at \(data.count) right now)")
            data = data.suffix(150)
        }
        
        persist()
        attemptSync()
        delegate?.dataStoreDidUpdate()
    }
    
    func attemptSync() {
        // Search through our set, looking for any data that hasn't been synced yet
        var unsynced = data.filter { $0.submitted == false }
        
        if unsynced.count == 0 {
            print("No new data to sync")
            return
        }
        
        let willBatchUpload = unsynced.count > 100
        if willBatchUpload {
            print("Syncing in batch of 100")
            unsynced = Array(unsynced.prefix(100))
        }
        
        let payload = [
            "data": unsynced,
        ]
        print("Attempting sync with: ", unsynced.count)
        
        var json: Data
        do {
            json = try JSONEncoder().encode(payload)
        } catch {
            return
        }
        let jsonString = String(data: json, encoding: .utf8)
        print("Sending payload: ", jsonString!)
        
        var req = URLRequest(url: URL(string: ApiUrl)!)
        req.httpMethod = "POST"
        req.setValue("application/json", forHTTPHeaderField: "Content-Type")
        req.setValue("", forHTTPHeaderField: "Authorization")
        req.httpBody = json
        
        Alamofire.request(req).responseJSON { response in
            if response.response?.statusCode != 200 {
                print("Error response, will retry later")
                return
            }
            
            let dict = unsynced.reduce(Dictionary<String, Bool>(), { (prev, datum) -> Dictionary<String, Bool> in
                var next = prev
                next[datum.id] = true
                return next
            })
            
            self.data.forEach({ (datum) in
                if dict[datum.id] == true {
                    datum.submitted = true
                }
            })
            
            print("Sync completed! Marked \(unsynced.count) items as submitted.")
            self.persist()
            
            // Recurse if there's more to do
            if willBatchUpload {
                self.attemptSync()
            }
        }
    }
    
    func persist() {
        // Save to disk
        do {
            let encoder = JSONEncoder()
            let jsonData = try encoder.encode(data)
            let jsonString = String(data: jsonData, encoding: .utf8)!
            try jsonString.write(to: storePath, atomically: false, encoding: .utf8)
        } catch {
            print("Could not write to file")
            return
        }
        print("Persisted \(data.count) items to disk")
    }
    
    func filter(type: DataFilterType) -> [Datum] {
        return data.filter({ (datum) -> Bool in
            return datum.data.toFilterType() == type
        })
    }
    
    func clear() {
        data.removeAll()
        persist()
    }
}
